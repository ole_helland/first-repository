﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager2 : MonoBehaviour

{

    public bool isSafetyOn = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyUp("space") && !isSafetyOn)
        {
            isSafetyOn = true;
            Debug.Log("You have turned the safety on");
        }

        else if (Input.GetKeyUp("space") && isSafetyOn)
        {
            isSafetyOn = false;
            Debug.Log("You have turned the safety off");
        }

        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    // Deklareringer av variabler man bruker.

    Material m_Material;

    //"False" fordi vi starter med variablene som "av"
    //Disse verdiene bruker vi senere for å ha en enkel
    //sjekk på å registrere at knappen er grønn eller trykket ned.

    bool isButtonGreen = false;
    bool isButtonDown = false;

    public GameObject checkBox;
    bool safetySwitch;

    // Start is called before the first frame update
    void Start()
    {
        m_Material = GetComponent<Renderer>().material;
    }

    void OnMouseDown()
    {
        safetySwitch = checkBox.GetComponent<InputManager2>().isSafetyOn;

        isButtonDown = true;

        // Når man trykker ned venstre museklikk
        // starter man coroutinen "OriginalColor"
        // dersom den ikke allerede er startet.

        if (isButtonGreen == false && !safetySwitch)
        {
            StartCoroutine(OriginalColor());
            m_Material.color = Color.green;
            isButtonGreen = true;
        }

        transform.localScale = new Vector3(1, 1, 1);
    }

    // Her startes coroutinen OriginalColor
    IEnumerator OriginalColor()
    {
        Debug.Log("Started the coroutine");
        yield return new WaitForSeconds(3);
        Debug.Log("Finished the coroutine");
        m_Material.color = Color.white;
        isButtonGreen = false;
    }

    void OnMouseOver()
    {
        // Hvis knappen ikke er trykket ned, så skal den gjøre knappen større

        if (isButtonDown == false)
        {
            transform.localScale = new Vector3(1.2f, 1.2f, 1);

        }
    }

    void OnMouseUp()
    {
        isButtonDown = false;
    }

    private void OnMouseExit()
    {
        transform.localScale = new Vector3(1, 1, 1);
    }
}
